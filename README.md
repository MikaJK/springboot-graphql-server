# springboot-graphql-server

## Schemas

GraphQL schema is a structured; JSON-like; file that describes the object types and operations
of a GraphQL service.

GraphQL schema files are stored into a `graphql` sub-folder under the `resources` folder with `graphqls` extension.

One GraphQL schema file can contain multiple objects and their operations, or each object and related
operations can be stored into separated files.

Following schemas (**Band**, **Member** and **Song**) are part of this example implementation. 

#### Band

Band is a type that has an identifier (**id**), two attributes (**name** and **genre**)
and two relationships (**members** and **songs**).

One band can have `0..n` members and each member can be part of `0..n` bands; i.e. `many-to-many`.

One band can compose and record `0..n` songs and each song is composed and recorded by `1` band;
i.e. `one-to-many`.

```
type Band {
     id: ID!,
     name: String!,
     genre: String,
     members: [Member],
     songs: [Song]
 }
 
 type Query {
     findBand(id: ID!): Band
     findBandByName(name: String!): Band
     findAllBands: [Band]!
     countBands: Int!
 }
 
 type Mutation {
     createBand(name: String!, genre: String!): Band
     updateBand(id: ID!, name: String!, genre: String!): Band
     addMember(bandId: ID!, memberId: ID!): Band
     removeMember(bandId: ID!, memberId: ID!): Band
 }
```
#### Member

Member is a type that has an identifier (**id**), two attributes (**firstName** and **lastName**) and
one relationship (**bands**).

```
type Member {
    id: ID!,
    firstName: String!,
    lastName: String!,
    bands: [Band]
}

extend type Query {
    findAllMembers: [Member]!
    countMembers: Int!
}

extend type Mutation {
    createMember(firstName: String!, lastName: String!): Member
}
```
#### Song

Song is a type that has an identifier (**id**), three attributes (**name**, **genre** and **duration**) and
one relationship (**band**).

```
type Song {
    id: ID!,
    name: String!,
    genre: String,
    duration: Int!,
    band: Band!
}

extend type Query {
    findAllSongs: [Song]!
    countSongs: Int!
}

extend type Mutation {
    createSong(name: String!, genre: String!, duration: Int!, bandId: ID!): Song
}
```


