package com.pikecape.springbootgraphqlserver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.pikecape.springbootgraphqlserver.model.Band;
import com.pikecape.springbootgraphqlserver.model.Song;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import com.pikecape.springbootgraphqlserver.repository.MemberRepository;
import com.pikecape.springbootgraphqlserver.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * SongMutation.
 *
 * @author Mika J. Korpela
 */
@Component
public class SongMutation implements GraphQLMutationResolver
{

    private BandRepository bandRepository;
    private MemberRepository memberRepository;
    private SongRepository songRepository;

    @Autowired
    public SongMutation(BandRepository bandRepository, MemberRepository memberRepository, SongRepository songRepository) {
        this.bandRepository = bandRepository;
        this.memberRepository = memberRepository;
        this.songRepository = songRepository;
    }

    /**
     * Create song.
     *
     * @param name
     * @param genre
     * @param duration
     * @param bandId
     * @return Song
     */
    @Transactional
    public Song createSong(String name, String genre, Integer duration, UUID bandId)
    {
        Song song = new Song();
        song.setBand(new Band(bandId));
        song.setName(name);
        song.setGenre(genre);
        song.setDuration(duration);
        return songRepository.save(song);
    }
}
