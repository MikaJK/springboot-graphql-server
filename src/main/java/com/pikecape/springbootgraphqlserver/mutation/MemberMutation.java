package com.pikecape.springbootgraphqlserver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.pikecape.springbootgraphqlserver.model.Member;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import com.pikecape.springbootgraphqlserver.repository.MemberRepository;
import com.pikecape.springbootgraphqlserver.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * MemberMutation.
 *
 * @author Mika J. Korpela
 */
@Component
public class MemberMutation implements GraphQLMutationResolver
{

    private BandRepository bandRepository;
    private MemberRepository memberRepository;
    private SongRepository songRepository;

    @Autowired
    public MemberMutation(BandRepository bandRepository, MemberRepository memberRepository, SongRepository songRepository) {
        this.bandRepository = bandRepository;
        this.memberRepository = memberRepository;
        this.songRepository = songRepository;
    }

    /**
     * Create member.
     *
     * @param firstName
     * @param lastName
     * @return Member
     */
    @Transactional
    public Member createMember(String firstName, String lastName)
    {
        Member member = new Member();
        member.setFirstName(firstName);
        member.setLastName(lastName);
        return memberRepository.save(member);
    }

    /**
     * Update member.
     *
     * @param id
     * @param firstName
     * @param lastName
     * @return Member
     */
    @Transactional
    public Member updateMember(UUID id, String firstName, String lastName)
    {
        Optional<Member> member = this.memberRepository.findById(id);

        if (member.isPresent()) {
            member.get().setFirstName(firstName);
            member.get().setLastName(lastName);
            return member.get();
        } else {
            return null;
        }
    }

    @Transactional
    public void deleteMember(UUID id)
    {

    }
}
