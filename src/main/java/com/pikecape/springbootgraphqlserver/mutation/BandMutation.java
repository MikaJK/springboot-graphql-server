package com.pikecape.springbootgraphqlserver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.pikecape.springbootgraphqlserver.model.Band;
import com.pikecape.springbootgraphqlserver.model.Genre;
import com.pikecape.springbootgraphqlserver.model.Member;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import com.pikecape.springbootgraphqlserver.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * BandMutation.
 *
 * @author Mika J. Korpela
 */
@Component
public class BandMutation implements GraphQLMutationResolver
{
    // attributes.
    private BandRepository bandRepository;
    private MemberRepository memberRepository;

    /**
     * Constructor.
     *
     * @param bandRepository
     * @param memberRepository
     */
    @Autowired
    public BandMutation(BandRepository bandRepository, MemberRepository memberRepository) {
        this.bandRepository = bandRepository;
        this.memberRepository = memberRepository;
    }

    /**
     * Create band.
     * @param name
     * @param genre
     * @return Band
     */
    @Transactional
    public Band createBand(final String name, final Genre genre)
    {
        Band band = new Band();
        band.setName(name);
        band.setGenre(genre);
        return this.bandRepository.save(band);
    }

    /**
     * Update band.
     *
     * @param id
     * @param name
     * @param genre
     * @return Band
     */
    @Transactional
    public Band updateBand(UUID id, String name, Genre genre)
    {
        Optional<Band> band = this.bandRepository.findById(id);

        if (band.isPresent()) {
            band.get().setName(name);
            band.get().setGenre(genre);
            return band.get();
        } else {
            return null;
        }
    }

    /**
     * Add band member.
     *
     * @param bandId
     * @param memberId
     * @return Band
     */
    @Transactional
    public Band addMember(UUID bandId, UUID memberId)
    {
        Optional<Band> band = this.bandRepository.findById(bandId);
        Optional<Member> member = this.memberRepository.findById(memberId);

        if (band.isPresent()) {
            if (member.isPresent()) {
                band.get().getMembers().add(member.get());
            }

            return band.get();
        }

        return null;
    }

    /**
     * Delete band.
     *
     * @param id
     */
    @Transactional
    public void deleteBand(UUID id)
    {
        this.bandRepository.deleteById(id);
    }

    /**
     * Remove member.
     *
     * @param bandId
     * @param memberId
     * @return Band
     */
    @Transactional
    public Band removeMember(UUID bandId, UUID memberId)
    {
        Optional<Band> band = this.bandRepository.findById(bandId);
        Optional<Member> member = this.memberRepository.findById(memberId);

        if (band.isPresent() && member.isPresent())
        {
            if (band.get().getMembers().contains(member.get())) {
                band.get().getMembers().remove(member.get());
            }

            return band.get();
        }

        return null;
    }
}
