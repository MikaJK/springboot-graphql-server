package com.pikecape.springbootgraphqlserver.resolver;

import com.pikecape.springbootgraphqlserver.model.Band;
import com.pikecape.springbootgraphqlserver.model.Member;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class MemberResolver
{
    @Autowired
    private BandRepository bandRepository;

    public MemberResolver(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }

    public Set<Band> getBands(Member member) {
        return member.getBands();
    }
}
