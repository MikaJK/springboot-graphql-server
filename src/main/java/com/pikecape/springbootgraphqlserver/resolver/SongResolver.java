package com.pikecape.springbootgraphqlserver.resolver;

import com.pikecape.springbootgraphqlserver.model.Band;
import com.pikecape.springbootgraphqlserver.model.Song;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SongResolver {
    @Autowired
    private BandRepository bandRepository;

    public SongResolver(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }

    public Band getBand(Song song) {
        return bandRepository.findById(song.getBand().getId()).orElseThrow(null);
    }
}
