package com.pikecape.springbootgraphqlserver.configuration;

import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean
    public TestRestTemplate testRestTemplate() {
        return new TestRestTemplate();
    }
}
