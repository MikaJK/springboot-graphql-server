package com.pikecape.springbootgraphqlserver.repository;

import com.pikecape.springbootgraphqlserver.model.Band;
import java.util.UUID;

import com.pikecape.springbootgraphqlserver.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BandRepository.
 * 
 * @author Mika J. Korpela
 */
@Repository
public interface BandRepository extends JpaRepository<Band, UUID> {
    public Band findByName(String name);
    public Band findByGenre(Genre genre);
}