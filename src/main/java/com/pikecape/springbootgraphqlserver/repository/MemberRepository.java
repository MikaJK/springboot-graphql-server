package com.pikecape.springbootgraphqlserver.repository;

import com.pikecape.springbootgraphqlserver.model.Member;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * MemberRepository.
 *
 * @author Mika J. Korpela
 */
@Repository
public interface MemberRepository extends JpaRepository<Member, UUID> {
}
