package com.pikecape.springbootgraphqlserver.repository;

import com.pikecape.springbootgraphqlserver.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * SongRepository.
 * 
 * @author Mika J. Korpela
 */
@Repository
public interface SongRepository extends JpaRepository<Song, UUID> {
}