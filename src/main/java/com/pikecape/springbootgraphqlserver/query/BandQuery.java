package com.pikecape.springbootgraphqlserver.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.pikecape.springbootgraphqlserver.model.Band;
import com.pikecape.springbootgraphqlserver.model.Member;
import com.pikecape.springbootgraphqlserver.model.Song;
import com.pikecape.springbootgraphqlserver.repository.MemberRepository;
import com.pikecape.springbootgraphqlserver.repository.BandRepository;
import com.pikecape.springbootgraphqlserver.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

/**
 * BandQuery.
 *
 * @author Mika J. Korpela
 */
@Component
public class BandQuery implements GraphQLQueryResolver
{
    private BandRepository bandRepository;
    private MemberRepository memberRepository;
    private SongRepository songRepository;

    @Autowired
    public BandQuery(BandRepository bandRepository, MemberRepository memberRepository, SongRepository songRepository) {
        this.bandRepository = bandRepository;
        this.memberRepository = memberRepository;
        this.songRepository = songRepository;
    }

    public Band findBand(UUID id) { return bandRepository.findById(id).orElse(null);}

    public Band findBandByName(String name) { return bandRepository.findByName(name);}

    public List<Band> findAllBands() {
        return bandRepository.findAll();
    }

    public List<Member> findAllMembers() {
        return memberRepository.findAll();
    }

    public List<Song> findAllSongs() {
        return songRepository.findAll();
    }

    public long countBands() {
        return bandRepository.count();
    }

    public long countMembers() {
        return memberRepository.count();
    }

    public long countSongs() {
        return songRepository.count();
    }
}