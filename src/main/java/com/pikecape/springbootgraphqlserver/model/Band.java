package com.pikecape.springbootgraphqlserver.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

/**
 * Band.
 *
 * @author Mika J. Korpela
 */
@Table(name = "bands")
@Entity
public class Band implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", nullable = false)
    private UUID id;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "genre", nullable = true)
    @Enumerated(EnumType.STRING)
    private Genre genre;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "band_member", joinColumns = @JoinColumn(name = "band_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "member_id", referencedColumnName = "id"))
    private Set<Member> members;

    @OneToMany(mappedBy = "band", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Song> songs = new ArrayList<>();

    /**
     * No arguments constructor.
     */
    public Band() {

    }

    /**
     * Custom constructor.
     *
     * @param id
     */
    public Band(UUID id) {
        this.id = id;
    }

    /**
     * Custom constructor.
     *
     * @param name
     * @param genre
     */
    public Band(String name, Genre genre, Set<Member> members) {
        this.name = name;
        this.genre = genre;
        this.members = members;
    }

    // getters and setters.
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() { return genre; }

    public void setGenre(Genre genre) { this.genre = genre; }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
