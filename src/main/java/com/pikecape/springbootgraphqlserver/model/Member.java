package com.pikecape.springbootgraphqlserver.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

/**
 * Member.
 * 
 * @author Mika J. Korpela
 */
@Table(name = "members")
@Entity
public class Member implements Serializable
{
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name="UUID", strategy="org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", nullable = false)
    private UUID id;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "last_name", nullable = true)
    private String lastName;
    
    @ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
    private Set<Band> bands;

    @ElementCollection(targetClass = Instrument.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "member_instrument")
    @Column(name = "instrumnets")
    private Set<Instrument> instruments;

    /**
     * No arguments constructor.
     */
    public Member() {
    }

    /**
     * Custom constructor.
     *
     * @param firstName
     * @param lastName
     */
    public Member(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Custom constructor.
     *
     * @param firstName
     * @param lastName
     * @param instruments
     */
    public Member(String firstName, String lastName, Set<Instrument> instruments) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.instruments = instruments;
    }

    // getters and setters.
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Band> getBands() {
        return bands;
    }

    public void setBands(Set<Band> bands) {
        this.bands = bands;
    }

    public Set<Instrument> getInstruments() { return instruments; }

    public void setInstruments(Set<Instrument> instruments) { this.instruments = instruments; }
}