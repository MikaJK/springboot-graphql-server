package com.pikecape.springbootgraphqlserver.model;

/**
 * Genre.
 *
 */
public enum Genre {
    ROCK,
    HARD_ROCK,
    PUNK_ROCK,
    METAL,
    POP
}
