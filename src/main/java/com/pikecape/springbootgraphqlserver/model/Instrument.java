package com.pikecape.springbootgraphqlserver.model;

/**
 * Instrument.
 *
 */
public enum Instrument {
    GUITAR,
    BASS,
    DRUMS,
    VOCALS,
    KEYBOARD
}
